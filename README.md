                                                        **Navigation bar exercise v1**

Food and Stuff, a regional supermarket chain, wants Catalyte to help them expand their online presence by building an e-commerce site. You have been tasked with the initial work for the navigation bar. It should allow customers to navigate to the two department pages (Food, Stuff) and view individual pages for select items. Customers should be able to "login" and "logout" (clicking a link is sufficient until another team builds out the authentication logic).

- Requirements:

1. Navigating to the site (localhost:3000) should show the login page with a navigation bar at the top including the company name (or logo) and a link labelled Login.
2. Given a user clicks the Login link, it should navigate to the Food department page (/food) and "login" the user. The page doesn't need to be built out at this point, but should include some text to let the user know which page they are on.
3. Given a user is logged in, the navbar should show the company name (or logo), a link to the Food department (/food) labelled Food, a link to the Stuff department (/stuff) labelled Stuff, and a link labelled Logout.
4. Given a user clicks the Stuff link, it should navigate to the Stuff department page (/stuff). The page should include some text to let the user know which page they are on and display at least 10 different items (stuff) a user can purchase.
5. Given a user selects an item, it should navigate to the individual item page (/stuff/:item) which shows the name of the selected item on the page.
6. Given a user clicks the Food link, it should navigate to the Food department page (/food). The page should include some text to let the user know which page they are on.
7. Given a user clicks the Logout link, it should "logout" the user and redirect to the login page (localhost:3000).
8. Navigating to a URL other than the login page, food department page, stuff department page, or an item page should show a 404 Not Found page.
9. Application should be broken down into multiple, logical components, stored in separate files.
10.Should include layout and component styling using CSS-in-JS or CSS modules or CSS libraries (ex. Bootstrap or Pure.css). (No fully styled 3rd party components)

- Stretch Requirements (These are optional requirements for additional practice):

1. Add styling to the navbar links that indicates which page the user is currently on.
