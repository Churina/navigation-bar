import React from 'react';
import './App.css';
import Food from './components/Food';
import Stuff from './components/Stuff';
import Navbar from './components/Navbar';
import { Route, Routes } from 'react-router-dom';
import Login from './components/Login';
import Logout from './components/Logout';
import Item from './components/Item';
import NotFound from './components/NotFound';


function App() {
  const [loggedIn, setLoggedIn] = React.useState(false);
  
  function login(){
    setLoggedIn(true);
  }

  function logout(){
    setLoggedIn(false);
  }

  return (
 
      <div className='App'>
        <Navbar loggedIn={loggedIn} />
        <Routes>
          <Route path="/" element={<Login login={login}/>} />
          <Route path="/food" element={<Food />} />
          <Route path="/stuff" element={<Stuff />} />
          <Route path="/stuff/:item" element={<Item />} />
          <Route path="/logout" element={<Logout logout={logout}/>} />
          <Route path="*" element={<NotFound />} />
        </Routes>
        {/* <button onClick={loginHandler}><Link to="/food">Log in</Link></button> */}
      </div>


  );
}

export default App;
