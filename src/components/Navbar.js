import React from 'react';
import { Link } from 'react-router-dom';


function Navbar({loggedIn}) {
    const navStyle = { color:'white', textDecoration: 'none', fontSize:'20px' }
  return (
    <nav >
      <h3>Food and Stuff</h3>
      
        
        {loggedIn &&
        <ul className='nav-links'>
          <Link style={navStyle} to="/food">
            <li className='active'>Food</li>
          </Link>
          <Link style={navStyle} to="/stuff">
            <li className='active'>Stuff</li>
          </Link>
          <Link style={navStyle} to="/logout">
            <li className='active'>Logout</li>
          </Link>
          </ul>
        }
        
     
    </nav>
  );
}

export default Navbar;
