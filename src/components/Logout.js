import React from 'react'
import { useNavigate } from 'react-router-dom';


function Logout({logout}) {
  let navigate = useNavigate();
  logout();
  return navigate("/");
}

export default Logout;