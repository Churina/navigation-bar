import React from 'react'
import { Link } from 'react-router-dom';

const items = ['item1','item2','item3','item4','item5','item6','item7','item8','item9','item10']

function Stuff() {

  const createItems = items.map(item => {
    return (
      <h2>
        <Link to={`/stuff/${item}`}>{item}</Link>
      </h2>
    )
   
  }) 

return (
    <div >
      <h1>Stuff Page</h1>
      {createItems}
    </div>
  );
}

export default Stuff;