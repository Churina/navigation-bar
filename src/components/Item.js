import React from 'react'
import { useParams } from 'react-router-dom';

function Item() {

let params = useParams();
  return (
    <h1>{params.item} Page</h1>
  );
}

export default Item;