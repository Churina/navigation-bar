import React from 'react'
import { Link } from 'react-router-dom';


function Login({login}) {
  return (
    <button onClick={login}><Link to="/food" style={{ textDecoration: 'none' }}>Log in</Link></button>
  );
}

export default Login;